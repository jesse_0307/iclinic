import Mock from 'mockjs'
const getListByDoctorId = Mock.mock(
  '/api/clinic/visit/listByDoctorId', 'post', (req, res) => {
    return {
      'code': 0,
      'msg': '',
      'data': [
        {
          'name': '小明',
          'sex': 1,
          'age': 8,
          'status': 1
        },
        {
          'name': '小王',
          'sex': 0,
          'age': 10,
          'status': 2
        }
      ]
    }
  }
)
const getDrugListByName = Mock.mock(
  '/api/clinic/prescription/findDrugListByName', 'post', (req, res) => {
    return {
      'code': 0,
      'msg': '',
      'data': [
        {
          'id': 1,
          'name': '当归',
          'specification': '10g/袋',
          'retailPrice': 12.00,
          'stock': 10
        },
        {
          'id': 2,
          'name': '大枣',
          'specification': '10g/袋',
          'retailPrice': 12.00,
          'stock': 20
        }
      ]
    }
  }
)
const getDiagnosisClassificationList = Mock.mock(
  '/api/precription/template/diagnosisClassification/list', 'post', (req, res) => {
    return {
      'code': 0,
      'msg': '',
      'data': [
        {
          'levelId': 1,
          'id': '0',
          'name': '未分类',
          'children': []
        },
        {
          'levelId': 1,
          'id': '1',
          'name': '内科',
          'children': [
            {
              'levelId': 2,
              'id': '8',
              'name': '呼吸系统疾病'
            },
            {
              'levelId': 2,
              'id': '9',
              'name': '心血管系统疾病'
            }
          ]
        },
        {
          'level': 1,
          'id': '2',
          'name': '外科',
          'children': [
            {
              'level': 2,
              'id': '17',
              'name': '心胸外科'
            },
            {
              'id': 18,
              'level': 2,
              'name': '普通外科'
            }
          ]
        }
      ]
    }
  }
)

const getListTemplatesByClassification = Mock.mock(
  '/api/precription/template/listTemplatesByClassification', 'post', (req, res) => {
    return {
      'code': 0,
      'msg': '',
      'data': [
        {
          'templateId': 1,
          'templateName': '麻黄汤'
        },
        {
          'templateId': 2,
          'templateName': '大青龙汤'
        }
      ]
    }
  }
)
const getFindOneChinese = Mock.mock(
  '/api/precription/template/findOneChinese', 'post', (req, res) => {
    return {
      'code': 0,
      'msg': '',
      'data': {
        'diagnosisClassification': '五官科--咽喉疾病',
        'templateName': '伤食型腹泻（秋季腹泻）',
        'mainSymptoms': '伤食型腹泻（秋季腹泻）',
        'templateDesc': '药物不能控制者，建议上一级医疗单位就诊。',
        'diurnalCount': 1,
        'duration': 10,
        'frequency': 1,
        'usage': 1,
        'drugList': [
          {
            'id': 1,
            'name': '苏叶',
            'totalDosage': '6',
            'unit': 'g',
            'unitPrice': 0.5
          },
          {
            'id': 2,
            'name': '杏仁',
            'totalDosage': '5',
            'unit': 'g',
            'unitPrice': 1.5
          },
          {
            'id': 3,
            'name': '杏仁',
            'totalDosage': '5',
            'unit': 'g',
            'unitPrice': 1.5
          },
          {
            'id': 4,
            'name': '杏仁',
            'totalDosage': '5',
            'unit': 'g',
            'unitPrice': 1.5
          }
        ]
      }
    }
  }
)
const getFindOneWestern = Mock.mock(
  '/api/precription/template/findOneWestern', 'post', (req, res) => {
    return {
      'code': 0,
      'msg': '',
      'data': {
        'diagnosisClassification': '五官科--咽喉疾病',
        'templateName': '伤食型腹泻（秋季腹泻）',
        'mainSymptoms': '伤食型腹泻（秋季腹泻）',
        'templateDesc': '药物不能控制者，建议上一级医疗单位就诊。',
        'drugList': [
          {
            'id': 1,
            'name': '湿贴',
            'singleDosage': '6',
            'singleUnit': '粒',
            'usage': '口服',
            'frequency': '一天三次',
            'daysNum': 2,
            'totalDosage': 5,
            'totalUnit': '盒',
            'unitPrice': 10,
            'needSkinTest': true
          },
          {
            'id': 2,
            'name': '小柴胡颗粒',
            'singleDosage': '10',
            'singleUnit': '粒',
            'usage': '口服',
            'frequency': '一天三次',
            'daysNum': 2,
            'totalDosage': 5,
            'totalUnit': '盒',
            'unitPrice': 20,
            'needSkinTest': false
          }
        ]
      }
    }
  }
)
const getFindOnePlaster = Mock.mock(
  '/api/precription/template/findOnePlaster', 'post', (req, res) => {
    return {
      'code': 0,
      'msg': '',
      'data': {
        'diagnosisClassification': '五官科--咽喉疾病',
        'templateName': '伤食型腹泻（秋季腹泻）',
        'mainSymptoms': '伤食型腹泻（秋季腹泻）',
        'templateDesc': '药物不能控制者，建议上一级医疗单位就诊。',
        'patchList': [
          {
            'usage': 3,
            'concoctionApproach': 2,
            'acupoint': '大椎,神阙',
            'duration': 6,
            'quatity': 1,
            'drugList': [
              {
                'id': 1,
                'name': '芒硝',
                'dosage': 1,
                'unit': 'g',
                'unitPrice': 1
              },
              {
                'id': 2,
                'name': '大黄',
                'dosage': 10,
                'unit': 'mg',
                'unitPrice': 2
              }
            ]
          },
          {
            'usage': 1,
            'concoctionApproach': 1,
            'acupoint': '神阙',
            'duration': 6,
            'quatity': 2,
            'drugList': [
              {
                'id': 3,
                'name': '焦三仙',
                'dosage': 1,
                'unit': 'g',
                'unitPrice': 2
              },
              {
                'id': 4,
                'name': '陈皮',
                'dosage': 10,
                'unit': 'mg',
                'unitPrice': 2
              }
            ]
          }
        ]
      }
    }
  }
)

export default {
  getListByDoctorId,
  getDrugListByName,
  getDiagnosisClassificationList,
  getListTemplatesByClassification,
  getFindOneChinese,
  getFindOneWestern,
  getFindOnePlaster
}
