import Vue from 'vue'
import Router from 'vue-router'
// import index from '@/components/index'
import todayService from '@/pages/service/todayService'
import newService from '@/pages/service/newService'

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'index',
    //   component: index
    // },
    {
      path: '/todayService',
      name: 'todayService',
      component: todayService
    },
    {
      path: '/newService',
      name: 'newService',
      component: newService
    }
  ]
})
