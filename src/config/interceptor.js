/**
 * axios 拦截器全局配置
 @author caiweili
 @date 2018/4/13
 */
import axios from 'axios'
// 样式框架
import iView from 'iview'

axios.defaults.timeout = 10000
axios.defaults.baseURL = ''

// Request参数配置
/**
 *
 */
axios.interceptors.request.use(config => {
  // console.log(config)
  // 返回配置
  return config
})

// Response配置
axios.interceptors.response.use(function (response) {
  if (response.status !== 200) {
    iView.Message.error({
      top: 50,
      duration: 5,
      content: '错误提示'
    })
  }
  // Do something with response data
  return response
}, function (error) {
  // Do something with response error
  return Promise.reject(error)
})
