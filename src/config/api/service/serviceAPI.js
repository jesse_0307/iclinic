const service = {
  getListByDoctorId: '/api/clinic/visit/listByDoctorId',
  // 药品列表
  getDrugListByName: '/api/clinic/prescription/findDrugListByName',
  // 门诊科室列表
  getDiagnosisClassificationList: '/api/precription/template/diagnosisClassification/list',
  // 处方列表
  getListTemplatesByClassification: '/api/precription/template/listTemplatesByClassification',
  // 中草药处方详情
  getFindOneChinese: '/api/precription/template/findOneChinese',
  // 西药处方详情
  getFindOneWestern: '/api/precription/template/findOneWestern',
  // 贴敷处方详情
  getFindOnePlaster: '/api/precription/template/findOnePlaster',
  // 保存病历
  saveRecord: '/api/clinic/visit/save'
}

export default service
