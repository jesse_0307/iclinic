import axios from 'axios'
let postMethod = (url, params) => {
  return axios({
    url,
    method: 'post',
    data: {...params},
    transformRequest: [function (data) {
      // Do whatever you want to transform the data
      let ret = ''
      for (let it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

import getService from '../api/service/serviceAPI'

// 查询我的接诊记录列表
export const getListByDoctorId = async ({patientName, beginDate, endDate}) => {
  return postMethod(getService.getListByDoctorId, {patientName, beginDate, endDate})
}
// 查询我的接诊记录列表
export const getDrugListByName = async ({durgName}) => {
  return postMethod(getService.getDrugListByName, {durgName})
}

// 查询我的接诊记录列表
export const getDiagnosisClassificationList = async () => {
  return postMethod(getService.getDiagnosisClassificationList, {})
}
// 查询我的接诊记录列表
export const getListTemplatesByClassification = async ({diagnosisClassificationId, precriptionType}) => {
  return postMethod(getService.getListTemplatesByClassification, {diagnosisClassificationId, precriptionType})
}
// 查询中药处方详情
export const getFindOneChinese = async ({templateId}) => {
  return postMethod(getService.getFindOneChinese, {templateId})
}
// 查询西药处方详情
export const getFindOneWestern = async ({templateId}) => {
  return postMethod(getService.getFindOneWestern, {templateId})
}
// 查询贴敷处方详情
export const getFindOnePlaster = async ({templateId}) => {
  return postMethod(getService.getFindOnePlaster, {templateId})
}
// 保存病历
export const saveRecord = async ({patientInfo, allergicHistory, chiefComplaint, hpi, examinationInfo, diagnosis, tcmSyndrome, treatmentPrinciple, handlingSuggestion}) => {
  return postMethod(getService.saveRecord, {patientInfo, allergicHistory, chiefComplaint, hpi, examinationInfo, diagnosis, tcmSyndrome, treatmentPrinciple, handlingSuggestion})
}

export default {
  getListByDoctorId,
  getDrugListByName,
  getDiagnosisClassificationList,
  getListTemplatesByClassification,
  getFindOneChinese,
  getFindOneWestern,
  getFindOnePlaster,
  saveRecord
}
