// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router/index'
import iView from 'iview'
import mockData from '../src/mock/index'
import 'iview/dist/styles/iview.css'

// axios全局请求拦截器
import './config/interceptor.js'

Vue.config.productionTip = false
Vue.use(iView)
Vue.use(mockData)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
